package fr.grunberg.stringcalculator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Calculates numbers as strings
 * @author Thomas Grunberg
 */
public class StringCalculator {
	
	/**
	 * Adds numbers contained in the String parameter
	 * @param numbers the numbers to add, with separator
	 * @return the sum
	 */
	public static int Add(String numbers) {
		// If this is not sum material
		if(numbers == null || numbers.isEmpty())
			return 0;
		
		// Defining the separators
		String separatorRegex = getSeparatorsRegex(numbers);

		// Badly designed separator
		if(separatorRegex == null || separatorRegex.isEmpty())
			return 0;
		
		// Separator in the beginning or end
		if(Arrays.stream(getSeparators(numbers))
			.anyMatch(separator -> {return numbers.startsWith(separator) || numbers.endsWith(separator);})
			)
			return 0;
		
		String numbersWithoutSeparatorsHeader = numbers;
		// Remove the separator header/definition
		if(numbers.startsWith("//") && numbers.contains("\n"))
			numbersWithoutSeparatorsHeader = numbers.substring(numbers.indexOf("\n")+1);
		// Split using the separator
		String[] splitNumbers = numbersWithoutSeparatorsHeader.split(separatorRegex);
		
		// Add the number
		int total = 0;
		List<Integer> negativeIntegersFound = new ArrayList<Integer>();
		
		try {
			total = Arrays.stream(splitNumbers)
					.mapToInt(number -> Integer.parseInt(number))
					.filter(numberParsed -> numberParsed <= 1000)
					.peek(numberParsed -> {
						if(numberParsed < 0)
							negativeIntegersFound.add(numberParsed);
						})
					.sum()
					;
		}
		catch(NumberFormatException e) {
			return 0;
		}
		
		// Throw an exception for negative numbers
		if(negativeIntegersFound != null && negativeIntegersFound.size() > 0) {
			String listOfNegativeNumbers = 
				negativeIntegersFound.stream()
					.map(negativeInteger -> String.valueOf(negativeInteger))
					.collect(Collectors.joining(", "))
					;
			throw new RuntimeException("negatives not allowed : " + listOfNegativeNumbers);
		}
		
		// Returns the total
		return total;
	}
	
	/**
	 * Gets the individual separators
	 * @return
	 */
	private static String[] getSeparators(String input) {
		if(input.startsWith("//") && input.contains("\n")) {
			String splitInput[] = input.split("\n");
			String separatorInput = splitInput[0];
			if(separatorInput.startsWith("//[") && separatorInput.endsWith("]")) {
				// Bracketed separators
				String bracketedSeparator = separatorInput.substring(3, separatorInput.length()-1);
				// Single separator
				if(!bracketedSeparator.contains("["))
					return new String[] {bracketedSeparator};
				// Multiple separators
				String[] bracketedSeparators = bracketedSeparator.split("\\[");
				bracketedSeparators = Arrays.stream(bracketedSeparators)
						.map(s -> s.replaceAll("\\[", "").replaceAll("\\]", ""))
						.toArray(String[]::new)
						;
				return bracketedSeparators;
			}
			// Regular separators
			else
				return new String[] {separatorInput.substring(2)};
		}
		else
			{
			String separators[] = new String[] {",", "\n"};
			return separators;
		}
		
	}
	/**
	 * Gets the separators as a regex
	 * @return
	 */
	private static String getSeparatorsRegex(String input) {
		String[] separators = getSeparators(input);
		if(separators == null || separators.length == 0)
			return null;
		String separatorRegex = 
			Arrays.stream(getSeparators(input))
				.map(separator -> separator.replaceAll("[*]", "\\[*]").replaceAll("[+]", "\\[+]"))
				.collect(Collectors.joining("|"))
				;
				
		return "(" + separatorRegex + ")";
	}
}
