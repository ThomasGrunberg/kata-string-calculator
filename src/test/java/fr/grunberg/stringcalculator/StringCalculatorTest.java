package fr.grunberg.stringcalculator;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests JUnit 
 * @author Thomas Grunberg
 *
 */
public class StringCalculatorTest {
	@Test
	public void Step1() {
		assertEquals(0, StringCalculator.Add((String)null));
		assertEquals(0, StringCalculator.Add(""));
		assertEquals(1, StringCalculator.Add("1"));
		assertEquals(16, StringCalculator.Add("16"));
		assertEquals(3, StringCalculator.Add("2,1"));
		assertEquals(7, StringCalculator.Add("3,4"));
	}

	@Test
	public void Step2() {
		assertEquals(10, StringCalculator.Add("3,4,1,2"));
		assertEquals(220, StringCalculator.Add("3,4,12,200,1"));
	}

	@Test
	public void Step3() {
		assertEquals(6, StringCalculator.Add("1\n2,3"));
		assertEquals(0, StringCalculator.Add("1,\n"));
		assertEquals(220, StringCalculator.Add("3,4\n12,200\n1"));
	}

	@Test
	public void Step4() {
		assertEquals(3, StringCalculator.Add("//;\n1;2"));
		assertEquals(0, StringCalculator.Add("//,\n1;2"));
		assertEquals(229, StringCalculator.Add("//;\n1;2;10;16;200"));
	}

	@Test
	public void Step5() {
	    Exception exception1 = assertThrows(RuntimeException.class, () -> {
	    	StringCalculator.Add("//;\n1;-2");
	    });
	    assertNotNull(exception1.getMessage());
	    assertTrue(exception1.getMessage().contains("negatives not allowed"));
	    assertTrue(exception1.getMessage().contains("-2"));

	    Exception exception2 = assertThrows(RuntimeException.class, () -> {
	    	StringCalculator.Add("//;\n-1;2");
	    });
	    assertNotNull(exception2.getMessage());
	    assertTrue(exception2.getMessage().contains("negatives not allowed"));
	    assertTrue(exception2.getMessage().contains("-1"));

	    Exception exception3 = assertThrows(RuntimeException.class, () -> {
	    	StringCalculator.Add("//;\n1;-2;10;-16;200");
	    });
	    assertNotNull(exception3.getMessage());
	    assertTrue(exception3.getMessage().contains("negatives not allowed"));
	    assertTrue(exception3.getMessage().contains("-2"));
	    assertTrue(exception3.getMessage().contains("-16"));

	}

	@Test
	public void Step6() {
		assertEquals(2, StringCalculator.Add("//;\n1001;2"));
		assertEquals(201, StringCalculator.Add("//;\n1;200"));
	}

	@Test
	public void Step7() {
		assertEquals(6, StringCalculator.Add("//[***]\n1***2***3"));
	}

	@Test
	public void Step8() {
		assertEquals(6, StringCalculator.Add("//[*][%]\n1*2%3"));
	}

	@Test
	public void Step9() {
		assertEquals(6, StringCalculator.Add("//[**][%]\n1**2%3"));
		assertEquals(33, StringCalculator.Add("//[,,,][%]\n1,,,2%30"));
		assertEquals(23, StringCalculator.Add("//[,=,][%]\n1,=,2%20"));
	}
}
